#pragma once

//GLEW for OpenGL access
#define GLEW_STATIC
#include <GL/glew.h>

//GLM for mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include <string>
#include <map>

#include "shader.h"

struct Material {
	glm::vec4 ambientReflectivity;
	glm::vec4 diffuseReflectivity;
	glm::vec4 specularReflectivity;
	float shininess;
};

struct Mesh {
	std::vector<glm::vec4> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texcoords;
	std::vector<GLushort> elements;
};

//bounding box info
struct AABB {
	AABB(glm::vec3 cd = glm::vec3(0.0,0.0,0.0), float wX = 0.0, float wY = 0.0, float wZ = 0.0)
		: centre(cd), halfWidthX(wX), halfWidthY(wY), halfWidthZ(wZ) {}
	glm::vec3 centre;
	float halfWidthX;
	float halfWidthY;
	float halfWidthZ;
};

class VisibleObject {
public:
	VisibleObject(std::string name, Mesh* mesh, Material* material)
		: mName(name), mMesh(mesh), mMaterial(material), mIsVisible(true) {};

	void sendObjectToGPU();
	void render();
	bool isVisible() { return mIsVisible; }
	void setVisibility(bool visibility) { mIsVisible = visibility; }

	void setMesh(Mesh* mesh) { mMesh = mesh; }

	void setMaterial(glm::vec4 ambient, glm::vec4 diffuse, glm::vec4 specular, float shininess)
	{
		mMaterial->ambientReflectivity = ambient;
		mMaterial->diffuseReflectivity = diffuse;
		mMaterial->specularReflectivity = specular;
		mMaterial->shininess = shininess;
	}

	std::string getName() { return mName; }

	void setAmbientReflectivity(glm::vec4 ambientReflectivity) { mMaterial->ambientReflectivity = ambientReflectivity; }
	glm::vec4 getAmbientReflectivity() { return mMaterial->ambientReflectivity; }
	void setdiffuseReflectivity(glm::vec4 diffuseReflectivity) { mMaterial->diffuseReflectivity = diffuseReflectivity; }
	glm::vec4 getDiffuseReflectivity() { return mMaterial->diffuseReflectivity; }
	void setspecularReflectivity(glm::vec4 specularReflectivity) { mMaterial->specularReflectivity = specularReflectivity; }
	glm::vec4 getSpecularReflectivity() { return mMaterial->specularReflectivity; }
	void setShininess(float shininess) { mMaterial->shininess = shininess; }
	float getShininess() { return mMaterial->shininess; }

	void setBuffers(GLuint vertexbufferID, GLuint normalBufferID, GLuint elementBufferID)
	{
		mVertexBufferID = vertexbufferID; mNormalBufferID = normalBufferID; mElementBufferID = elementBufferID;
	}

	glm::vec3 getMeshCentroid();
	float getBoundingSphereRadius();
	AABB getBoundingBox();

	void translateMesh(glm::vec3 disp);

	glm::mat4 getModelTransform() { return mModelTransform; }
	void setModelTransform(glm::mat4 tm) { mModelTransform = tm; }

	glm::mat4 getModelMovement() { return mMovement; }
	void setModelMovement(glm::mat4 m) { mMovement = m; }

	GLuint getVertexBufferID() { return mVertexBufferID; }
	GLuint getNormalBufferID() { return mNormalBufferID; }
	GLuint getElementBufferID() { return mElementBufferID; }

	void move(glm::vec3 d);
private:
	std::string mName;
	bool mIsVisible;
	std::string mMeshSource;

	glm::mat4 mModelTransform;
	glm::mat4 mMovement;

	Mesh *mMesh;
	Material *mMaterial;

	GLuint mVertexBufferID, mNormalBufferID, mElementBufferID;
};