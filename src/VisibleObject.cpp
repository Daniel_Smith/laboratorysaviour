#include "VisibleObject.h"

void VisibleObject::sendObjectToGPU() {
	//buffers to hold mesh data
	glGenBuffers(1, &mVertexBufferID);
	glGenBuffers(1, &mNormalBufferID);
	glGenBuffers(1, &mElementBufferID);

	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	size_t dataSize = mMesh->vertices.size() * sizeof(mMesh->vertices[0]);
	glBufferData(GL_ARRAY_BUFFER, dataSize, mMesh->vertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, mNormalBufferID);
	size_t ndataSize = mMesh->normals.size() * sizeof(mMesh->normals[0]);
	glBufferData(GL_ARRAY_BUFFER, ndataSize, mMesh->normals.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mElementBufferID);
	size_t indicesSize = mMesh->elements.size() * sizeof(mMesh->elements[0]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, mMesh->elements.data(), GL_STATIC_DRAW);
}

void VisibleObject::render() {
	glBindBuffer(GL_ARRAY_BUFFER, mVertexBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, mNormalBufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mElementBufferID);
	glDrawElements(GL_TRIANGLES, mMesh->elements.size(), GL_UNSIGNED_SHORT, 0);
}

glm::vec3 VisibleObject::getMeshCentroid() {
	glm::vec3 c(0.0f, 0.0f, 0.0f);
	size_t nVertices = mMesh->vertices.size();
	for (size_t i = 0; i < nVertices; ++i) {
		c += glm::vec3(mMesh->vertices[i]);
	}
	return c / (float)nVertices;
}

float VisibleObject::getBoundingSphereRadius() {
	glm::vec3 cd = getMeshCentroid();
	float maxR = 0.0;
	size_t nVertices = mMesh->vertices.size();
	for (size_t i = 0; i < nVertices; ++i) {
		glm::vec4 v = mMesh->vertices[i] - glm::vec4(cd, 0.0f);
		float dx = v.x;
		float dy = v.y;
		float dz = v.z;
		if (dx > maxR) maxR = dx;
		if (dy > maxR) maxR = dy;
		if (dz > maxR) maxR = dz;
	}
	return maxR;
}

AABB VisibleObject::getBoundingBox() {
	glm::vec3 cd = getMeshCentroid();
	float maxX = 0.0;
	float maxY = 0.0;
	float maxZ = 0.0;
	size_t nVertices = mMesh->vertices.size();
	for (size_t i = 0; i < nVertices; ++i) {
		float dx = abs(mMesh->vertices[i].x - cd.x);
		float dy = abs(mMesh->vertices[i].y - cd.y);
		float dz = abs(mMesh->vertices[i].z - cd.z);
		if (dx > maxX) maxX = dx;
		if (dy > maxY) maxY = dy;
		if (dz > maxZ) maxZ = dz;
	}
	return AABB(cd, maxX, maxY, maxZ);
}

void VisibleObject::translateMesh(glm::vec3 disp) {
	//change vertices by amount given
	size_t nVertices = mMesh->vertices.size();
	for (size_t i = 0; i < nVertices; ++i) {
		glm::vec4 v = mMesh->vertices[i];
		mMesh->vertices[i] = v + glm::vec4(disp, 0.0f);
	}
}

void VisibleObject::move(glm::vec3 d) {
	glm::mat4 transMat = glm::translate(glm::mat4(), d);
	mMovement = transMat;
}