#pragma once

//include GLEW for OpenGl access
#define GLEW_STATIC
#include <GL\glew.h>
#include "Game.h"

//include SDL
#include "SDL.h"
#include "SDL_opengl.h"

//include GLM
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include <vector>
#include <string>
#include <map>

#include <functional>

#include "btBulletCollisionCommon.h"
#include "BulletDynamics\Dynamics\btDiscreteDynamicsWorld.h"
#include "BulletDynamics\Dynamics\btRigidBody.h"

#include "VisibleObject.h"

//#if (_Win32)
#include <windows.h>
//#endif

struct Light {
	glm::vec4 position;
	glm::vec4 diffuseColour;
	glm::vec4 ambientColour;
	glm::vec4 specularColour;
};

class KeyHandler {
public:
	KeyHandler(std::function<void(float)> f) : action(f) {}
	void operator() (float d) { action(d); }
	void run(float d) { action(d); }
private:
	std::function<void(float)> action;
};

struct MyContactResultCallback : public btDiscreteDynamicsWorld::ContactResultCallback {
	btScalar virtual addSingleResult(btManifoldPoint& cp,
		const btCollisionObjectWrapper* colObj0Wrap,
		int partId0,
		int index0,
		const btCollisionObjectWrapper* colObj1Wrap,
		int partId1,
		int index1)
	{
		const btRigidBody* rb0 = btRigidBody::upcast(colObj0Wrap->getCollisionObject());
		const btRigidBody* rb1 = btRigidBody::upcast(colObj1Wrap->getCollisionObject());
		std::string rb0Name = (*bodyNames).at(const_cast<btRigidBody*>(rb0));
		std::string rb1Name = (*bodyNames).at(const_cast<btRigidBody*>(rb1));

		// Not interested  in collision with floor!
		if (rb1Name != "Floor") {
			std::cout << "Collision detected" << std::endl;

			std::cout << rb0Name << std::endl;
			std::cout << colObj0Wrap->getCollisionShape()->getName() << std::endl;

			std::cout << rb1Name << std::endl;
			std::cout << colObj1Wrap->getCollisionShape()->getName() << std::endl;

			collisionCount++;

			std::cout << "Num collisions : " << collisionCount << "\n" << std::endl;

			if (rb1Name == "PAtom" && rb0Name == "Atom" || rb1Name == "Atom"&& rb0Name == "PAtom") {
				atomsCollided = true;
				PlaySound("assets/Bell-ding.wav", NULL, SND_FILENAME | SND_ASYNC);
			}

		}
		return 0;
	}
	int collisionCount = 0;
	bool atomsCollided = false;
	std::map<btRigidBody*, std::string>* bodyNames;
};

class Game
{
public:
	void initialise();
	void run();
	void shutdown();
	
protected:
	virtual void readConfigFile();
	virtual void bindKeyboard();

	virtual void loadAssets();

	virtual void update(SDL_Keycode aKey, float delta);
	virtual void render();

	std::vector<VisibleObject*> mVisibleWorld;

	SDL_Window* window;
	SDL_GLContext context;

	int screenWidth, screenHeight;

	glm::mat4 view;
	glm::mat4 projection;

	Light theLight;



	ShaderProgram * defaultShader;
private:
	std::string configFile;
	std::vector<std::string> assetFiles;
	

	std::map<std::string, std::string> keyBindings;
	std::map<std::string, KeyHandler*> commandHandler;

	glm::vec3 cameraPos;
	glm::vec3 cameraOffset;
	glm::vec3 cameraLookDirection;
	glm::vec3 cameraUp;

	std::string physicsFilename;
	bool isPhysicsPaused = false;
	glm::mat4 phys2VisTransform;
	bool correctForBullet = true;

	btDiscreteDynamicsWorld* thePhysicsWorld;
	btRigidBody* pAtom;
	btVector3 pAtomStartPos;
	btQuaternion pAtomStartOrientation;
	btRigidBody* atom;
	btVector3 atomStartPos;
	btQuaternion atomStartOrientation;
	btRigidBody* particle1;
	btVector3 particle1StartPos;
	btQuaternion particle1StartOrientation;
	btRigidBody* particle2;
	btVector3 particle2StartPos;
	btQuaternion particle2StartOrientation;

	MyContactResultCallback collisionCallback;

	std::map<btRigidBody*, std::string> rigidBodyNames;

	void turnCamLeft();
	void turnCamRight();
	void chargeHit();
	double chargeStartTime;
	float totalChargeTime;
	bool spaceKeyHeld = false;
	double chargeForce = 0.01;
	double inputDelay = 0;

	void atomCollision();
	
};