#include "Game.h"

#include "BulletDynamics\ConstraintSolver\btSequentialImpulseConstraintSolver.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


#include <iostream>
#include <fstream>
#include <sstream>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <Extras\Serialize\BulletWorldImporter\btBulletWorldImporter.h>

using namespace std;

//**********Game**********
void Game::initialise() {
	

	//Initialise SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	screenWidth = 1280;
	screenHeight = 720;

	//Set up window and context
	cout << "Creating OpenGL window and context";
	window = SDL_CreateWindow("OpenGL", 50, 50, screenWidth, screenHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	context = SDL_GL_CreateContext(window);

	//Initialise GLEW
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();
	if (glewError != GLEW_OK) {
		cout << "GLEW Error" << glewGetString(GLEW_VERSION) << endl;
	}

	//Config file
	configFile = "assets/config.txt";
	//Read config file
	readConfigFile();

	//Load Assets
	loadAssets();

	//bind keyboard
	bindKeyboard();

	//OpenGL buffers for mesh objects
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	//send objects to GPU
	for (unsigned int i = 0; i < mVisibleWorld.size(); ++i) {
		mVisibleWorld[i]->sendObjectToGPU();
	}

	//set up a light
	theLight.position = glm::vec4(-5.0f, 5.0f, -4.0f, 1.0f);
	theLight.ambientColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	theLight.diffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	theLight.specularColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	//View and Projection matrices
	view = glm::lookAt(glm::vec3(0.0, 10.0, 20.0), glm::vec3(0.0, 0.0, -4.0), glm::vec3(0.0, 1.0, 0.0));
	projection = glm::perspective(45.0f, 1.0f*screenWidth / screenHeight, 1.0f, 100.0f);

	//shaders
	try {
		defaultShader = new ShaderProgram();
		defaultShader->initFromFiles("assets/default.vert", "assets/default.frag");
		defaultShader->addAttribute("vPosition");
		defaultShader->addAttribute("vNormal");
		defaultShader->addUniform("P");
		defaultShader->addUniform("M");
		defaultShader->addUniform("V");
		defaultShader->addUniform("lightPosition");
		defaultShader->addUniform("ambientContrib");
		defaultShader->addUniform("diffuseContrib");
		defaultShader->addUniform("specularContrib");
		defaultShader->addUniform("shininess");

		defaultShader->use();
	}
	catch (const runtime_error& error) {
		cerr << "Error in shader processing!" << endl;
		cerr << error.what();
	}

	glEnable(GL_DEPTH_TEST);

	//Initialise physics world
	btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
	btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);
	btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();
	btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;
	thePhysicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

	isPhysicsPaused = true;

	//Associate visible and physics world

	btBulletWorldImporter* fileLoader = new btBulletWorldImporter(thePhysicsWorld);

	fileLoader->loadFile(physicsFilename.c_str());

	int numRigidBodies = fileLoader->getNumRigidBodies();
	for (int i = 0; i < numRigidBodies; i++) {
		btCollisionObject* coll = fileLoader->getRigidBodyByIndex(i);
		btRigidBody* body = btRigidBody::upcast(coll);
		string bodyName = fileLoader->getNameForPointer(body);
		cout << "Body name: " << bodyName << endl;
		rigidBodyNames[body] = bodyName;
		if (bodyName == "PAtom") {
			pAtom = body;
			pAtomStartPos = pAtom->getCenterOfMassPosition();
			pAtomStartOrientation = pAtom->getOrientation();
			pAtom->setRestitution(0.85);
			int cflag = pAtom->getCollisionFlags();
			pAtom->setActivationState(DISABLE_DEACTIVATION);
			pAtom->setDamping(1.0, 1.0);
			pAtom->setAngularFactor(0.0);
			//Camera
			cameraPos = glm::vec3(pAtomStartPos.getX(), pAtomStartPos.getY(), pAtomStartPos.getZ());
			cameraLookDirection = cameraPos;
			cameraOffset = glm::vec3(0.0f, 1.5f, 0.75f);
			cameraPos = cameraPos + cameraOffset;
			cameraUp = glm::vec3(0.0f, 0.0f, 1.0f);
		}
		if (bodyName == "Atom") {
			atom = body;
			atomStartPos = atom->getCenterOfMassPosition();
			atomStartOrientation = atom->getOrientation();
			atom->setRestitution(0.85);
		}
		if (bodyName == "Particle1") {
			particle1 = body;
			particle1StartPos = particle1->getCenterOfMassPosition();
			particle1StartOrientation = particle1->getOrientation();
			particle1->setRestitution(0.85);
		}
		if (bodyName == "Particle2") {
			particle2 = body;
			particle2StartPos = particle2->getCenterOfMassPosition();
			particle2StartOrientation = particle2->getOrientation();
			particle2->setRestitution(0.85);
		}
		body->setRestitution(0.85);
		collisionCallback.bodyNames = &rigidBodyNames;

	}
	thePhysicsWorld->setGravity(btVector3(0.0, 0.0, -9.8));

	//fix X and Y axes from .obj and bullet files
	phys2VisTransform = glm::mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, -1.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	isPhysicsPaused = false;
}

void Game::run() {
	SDL_Event windowEvent;
	float lastTime = SDL_GetTicks() / 1000.0f;
	while (true) {
		float current = SDL_GetTicks() / 1000.0f;
		float delta = current - lastTime;
		SDL_Keycode theKey = SDLK_UNKNOWN;

		if (SDL_PollEvent(&windowEvent)) {
			if (windowEvent.type == SDL_QUIT) break;
			if (windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_ESCAPE) break;

			if (windowEvent.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
				std::cout << "Window Resized." << std::endl;
				screenWidth = windowEvent.window.data1;
				screenHeight = windowEvent.window.data2;
				glViewport(0, 0, screenWidth, screenHeight);
			}

			if (inputDelay <= 0) {
				if (windowEvent.type == SDL_KEYDOWN) {
					theKey = windowEvent.key.keysym.sym;
					if (windowEvent.key.keysym.sym == SDLK_SPACE) {
						chargeStartTime = SDL_GetTicks();
						spaceKeyHeld = true;
					}
				}
				if (spaceKeyHeld) {
					totalChargeTime = totalChargeTime + delta;
					if (totalChargeTime > 0.3) totalChargeTime = 0.3;
					cout << totalChargeTime << endl;
				}
				if (windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_SPACE) {
					spaceKeyHeld = false;
					chargeForce = totalChargeTime / 30;
					chargeHit();
					inputDelay = 2;
				}
			}
			
		}
		if (inputDelay > 0) {
			chargeHit();
			inputDelay -= delta;
			cout << inputDelay << endl;
			if (inputDelay - delta <= 0) totalChargeTime = 0;
		}

		update(theKey, delta);

		render();

		lastTime = current;
	}
}

void Game::shutdown() {
	//Shutdown SDL
	SDL_GL_DeleteContext(context);
	SDL_Quit();
}

void Game::readConfigFile() {
	string line;
	string key;
	ifstream ifs(configFile, ifstream::in);

	while (ifs.good() && !ifs.eof() && getline(ifs, line)) {
		string configType = "";
		stringstream str(line);
		str >> configType;
		if (configType == "#") //line is a comment
			continue;
		if (configType == "Asset") { //name and path of an asset file
			string fname;
			str >> fname;
			size_t lastSlash = fname.find_last_of("/\\");
			assetFiles.push_back(fname);
		}
		if (configType == "Physics") { //name and path of physics file
			str >> physicsFilename;
		}
		else if (configType == "Key") { //key binding
			string key;
			string binding;
			str >> key >> binding;
			keyBindings[key] = binding;
		}

	}
}

void Game::bindKeyboard() {
	KeyHandler *lookLeft = new KeyHandler([this](float d) {
		turnCamLeft();
	});
	commandHandler["lookLeft"] = lookLeft;

	KeyHandler *lookRight = new KeyHandler([this](float d) {
		turnCamRight();
	});
	commandHandler["lookRight"] = lookRight;

	KeyHandler *chargeShot = new KeyHandler([this](float d) {
		chargeHit();
	});
	commandHandler["chargeShot"] = chargeShot;

	KeyHandler *togglePhysics = new KeyHandler([this](float d) {
		isPhysicsPaused = !isPhysicsPaused;
	});
	commandHandler["togglePhysics"] = togglePhysics;

	KeyHandler *correctBullet = new KeyHandler([this](float d) {
		correctForBullet = !correctForBullet;
	});
	commandHandler["correctBullet"] = correctBullet;
}

void Game::loadAssets() {
	//Process asset files
	Assimp::Importer importer;
	for (int i = 0; i < assetFiles.size(); ++i) {
		const aiScene* scene = importer.ReadFile(assetFiles[i], 0);
		if (!scene) {
			fprintf(stderr, importer.GetErrorString());
		}

		aiNode* rn = scene->mRootNode;

    #ifdef _DEBUG
		cout << "Mesh source: " << assetFiles[i] << endl;
		cout << "Root node: " << rn->mName.data << endl;
		cout << "Num children: " << rn->mNumChildren << endl;
		for (unsigned int i = 0; i < rn->mNumChildren; i++) {
			const aiNode* cn = rn->mChildren[i];
			cout << "C" << i << " Name: " << cn->mName.data << endl;
			cout << " Num children: " << cn->mNumChildren << endl;
			cout << " Num meshes: " << cn->mNumMeshes << endl;
			cout << " Mesh index: " << cn->mMeshes[0] << endl;
			int matIndex = scene->mMeshes[cn->mMeshes[0]]->mMaterialIndex;
			cout << " Mesh material index: " << matIndex << endl;
			aiString matname;
			scene->mMaterials[matIndex]->Get(AI_MATKEY_NAME, matname);
			cout << " Material Name: " << matname.data << endl;
			aiColor3D colorD(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, colorD);
			cout << " Material diffusive colour: " << colorD.r << "," << colorD.g << "," << colorD.b << endl;
			aiColor3D colorA(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_AMBIENT, colorA);
			cout << " Material ambient colour: " << colorA.r << "," << colorA.g << "," << colorA.b << endl;
			aiColor3D colorS(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_SPECULAR, colorS);
			cout << " Material specular colour: " << colorS.r << "," << colorS.g << "," << colorS.b << endl;
			float shininess = 0.0;
			scene->mMaterials[matIndex]->Get(AI_MATKEY_SHININESS, shininess);
			//returns value 4x too large
			cout << " Material specular shininess: " << (shininess / 4.0) << endl;
		}
    #endif
		
		for (unsigned int i = 0; i < rn->mNumChildren; i++) {
			const aiNode* cn = rn->mChildren[i];
			//Construct GameObject and extract mesh and material definitions
			string name = cn->mName.data;
			unsigned int meshIndex = cn->mMeshes[0];
			aiMesh* theSceneMesh = scene->mMeshes[meshIndex];

			Mesh * objMesh = new Mesh();

			//get vertices
			objMesh->vertices.reserve(theSceneMesh->mNumVertices);
			for (unsigned int i = 0; i < theSceneMesh->mNumVertices; i++) {
				aiVector3D pos = theSceneMesh->mVertices[i];
				objMesh->vertices.push_back(glm::vec4(pos.x, pos.y, pos.z, 1.0f));
			}

			//get normals
			if (theSceneMesh->HasNormals()) {
				objMesh->normals.reserve(theSceneMesh->mNumVertices);
				for (unsigned int i = 0; i < theSceneMesh->mNumVertices; i++) {
					aiVector3D n = theSceneMesh->mNormals[i];
					objMesh->normals.push_back(glm::vec3(n.x, n.y, n.z));
				}
			}

			//fill face indices
			if (theSceneMesh->HasFaces()) {
				objMesh->elements.reserve(3 * theSceneMesh->mNumFaces);
				for (unsigned int i = 0; i < theSceneMesh->mNumFaces; i++) {
					objMesh->elements.push_back(theSceneMesh->mFaces[i].mIndices[0]);
					objMesh->elements.push_back(theSceneMesh->mFaces[i].mIndices[1]);
					objMesh->elements.push_back(theSceneMesh->mFaces[i].mIndices[2]);
				}
			}

			//fill vertices texture coordinates
			if (theSceneMesh->mTextureCoords[0] != NULL) {
				objMesh->texcoords.reserve(theSceneMesh->mNumVertices);
				for (unsigned int i = 0; i < theSceneMesh->mNumVertices; i++) {
					aiVector3D UVW = theSceneMesh->mTextureCoords[0][i];
					objMesh->texcoords.push_back(glm::vec2(UVW.x, UVW.y));
				}
			}

			Material* objMaterial = new Material();

			//get associated material
			int matIndex = scene->mMeshes[meshIndex]->mMaterialIndex;

			aiColor3D colorA(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_AMBIENT, colorA);
			objMaterial->ambientReflectivity = glm::vec4(colorA.r, colorA.g, colorA.b, 1.0f);

			aiColor3D colorD(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_DIFFUSE, colorD);
			objMaterial->diffuseReflectivity = glm::vec4(colorD.r, colorD.g, colorD.b, 1.0f);

			aiColor3D colorS(0.f, 0.f, 0.f);
			scene->mMaterials[matIndex]->Get(AI_MATKEY_COLOR_SPECULAR, colorS);
			objMaterial->specularReflectivity = glm::vec4(colorS.r, colorS.g, colorS.b, 1.0f);
			float shininess = 0.0;
			scene->mMaterials[matIndex]->Get(AI_MATKEY_SHININESS, shininess);
			//returns a value 4x too large
			objMaterial->shininess = shininess / 4;

			//Add to world
			mVisibleWorld.push_back(new VisibleObject(name, objMesh, objMaterial));
		}

	}
}

void Game::update(SDL_Keycode akey, float delta) {
	if (akey != SDLK_UNKNOWN) {
		string kname = SDL_GetKeyName(akey);
		//run command bound to key
		if (keyBindings.find(kname) != keyBindings.end()) {
			string command = keyBindings[kname];
			(*commandHandler[command])(delta);
		}
	}

	//update physics world
	if (!isPhysicsPaused) {
		thePhysicsWorld->stepSimulation(delta, 10);
	}
}

void Game::render() {
	//Display model
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	for (int i = 0; i < mVisibleWorld.size(); ++i) {
		if (mVisibleWorld[i]->isVisible()) {
			if (mVisibleWorld[i]->getName() == "PAtom_Sphere") {
				btTransform pAtomTransform;
				pAtomTransform = pAtom->getWorldTransform();
				btVector3 pAtomPos = pAtomTransform.getOrigin();
				btQuaternion pAtomQuat = pAtomTransform.getRotation();
				btQuaternion tq = pAtomQuat * pAtomStartOrientation.inverse();
				btVector3 rotAxis = tq.getAxis();
				btScalar rot = tq.getAngle();

				glm::mat4 cubeTrans;
				cubeTrans = glm::translate(glm::mat4(), glm::vec3(pAtomPos.getX(), pAtomPos.getY(), pAtomPos.getZ()));
				cubeTrans = glm::rotate(cubeTrans, rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cubeTrans = glm::translate(cubeTrans, glm::vec3((-1.0f) * pAtomStartPos.getX(), (-1.0f) * pAtomStartPos.getY(), (-1.0f) * pAtomStartPos.getZ()));
				mVisibleWorld[i]->setModelMovement(cubeTrans);

				//update camera
				cameraPos = glm::vec3(pAtomPos.getX(), pAtomPos.getY(), pAtomPos.getZ());
				cameraLookDirection = cameraPos;
				glm::quat rq = glm::angleAxis(rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cameraPos = cameraPos + glm::rotate(rq, cameraOffset);
			}

			if (mVisibleWorld[i]->getName() == "Atom_Sphere.001") {
				btTransform atomTransform;
				atomTransform = atom->getWorldTransform();
				btVector3 atomPos = atomTransform.getOrigin();
				btQuaternion atomQuat = atomTransform.getRotation();
				btQuaternion tq = atomQuat * atomStartOrientation.inverse();
				btVector3 rotAxis = tq.getAxis();
				btScalar rot = tq.getAngle();

				glm::mat4 cubeTrans;
				cubeTrans = glm::translate(glm::mat4(), glm::vec3(atomPos.getX(), atomPos.getY(), atomPos.getZ()));
				cubeTrans = glm::rotate(cubeTrans, rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cubeTrans = glm::translate(cubeTrans, glm::vec3((-1.0f) * atomStartPos.getX(), (-1.0) * atomStartPos.getY(), (-1.0f) * atomStartPos.getZ()));
				mVisibleWorld[i]->setModelMovement(cubeTrans);
			}
			if (mVisibleWorld[i]->getName() == "Particle1_Sphere.002") {
				btTransform particle1Transform;
				particle1Transform = particle1->getWorldTransform();
				btVector3 particle1Pos = particle1Transform.getOrigin();
				btQuaternion particle1Quat = particle1Transform.getRotation();
				btQuaternion tq = particle1Quat * particle1StartOrientation.inverse();
				btVector3 rotAxis = tq.getAxis();
				btScalar rot = tq.getAngle();

				glm::mat4 cubeTrans;
				cubeTrans = glm::translate(glm::mat4(), glm::vec3(particle1Pos.getX(), particle1Pos.getY(), particle1Pos.getZ()));
				cubeTrans = glm::rotate(cubeTrans, rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cubeTrans = glm::translate(cubeTrans, glm::vec3((-1.0f) * particle1StartPos.getX(), (-1.0) * particle1StartPos.getY(), (-1.0f) * particle1StartPos.getZ()));
				mVisibleWorld[i]->setModelMovement(cubeTrans);
			}
			if (mVisibleWorld[i]->getName() == "Particle2_Sphere.003") {
				btTransform particle2Transform;
				particle2Transform = particle2->getWorldTransform();
				btVector3 particle2Pos = particle2Transform.getOrigin();
				btQuaternion particle2Quat = particle2Transform.getRotation();
				btQuaternion tq = particle2Quat * particle2StartOrientation.inverse();
				btVector3 rotAxis = tq.getAxis();
				btScalar rot = tq.getAngle();

				glm::mat4 cubeTrans;
				cubeTrans = glm::translate(glm::mat4(), glm::vec3(particle2Pos.getX(), particle2Pos.getY(), particle2Pos.getZ()));
				cubeTrans = glm::rotate(cubeTrans, rot, glm::vec3(rotAxis.getX(), rotAxis.getY(), rotAxis.getZ()));
				cubeTrans = glm::translate(cubeTrans, glm::vec3((-1.0f) * particle2StartPos.getX(), (-1.0) * particle2StartPos.getY(), (-1.0f) * particle2StartPos.getZ()));
				mVisibleWorld[i]->setModelMovement(cubeTrans);
			}

			//shader attributes and uniforms
			glEnableVertexAttribArray(defaultShader->attribute("vPosition"));
			glBindBuffer(GL_ARRAY_BUFFER, mVisibleWorld[i]->getVertexBufferID());
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(defaultShader->attribute("vNormal"));
			glBindBuffer(GL_ARRAY_BUFFER, mVisibleWorld[i]->getNormalBufferID());
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

			//uniforms for materials values
			glm::vec4 ambientContrib = theLight.ambientColour * mVisibleWorld[i]->getAmbientReflectivity();
			glm::vec4 diffuseContrib = theLight.diffuseColour  * mVisibleWorld[i]->getDiffuseReflectivity();
			glm::vec4 specularContrib = theLight.specularColour * mVisibleWorld[i]->getSpecularReflectivity();
			float shininess = mVisibleWorld[i]->getShininess();

			glUniform4fv(defaultShader->uniform("ambientContrib"), 1, glm::value_ptr(ambientContrib));
			glUniform4fv(defaultShader->uniform("diffuseContrib"), 1, glm::value_ptr(diffuseContrib));
			glUniform4fv(defaultShader->uniform("specularContrib"), 1, glm::value_ptr(specularContrib));
			glUniform1f(defaultShader->uniform("shininess"), shininess);

			//object model transform
			glm::mat4 mtb = mVisibleWorld[i]->getModelTransform();
			glm::mat4 mtm = mVisibleWorld[i]->getModelMovement();
			glm::mat4 mt = mtm * mtb;
			if (correctForBullet) mt = phys2VisTransform * mt;
			glUniformMatrix4fv(defaultShader->uniform("M"), 1, GL_FALSE, glm::value_ptr(mt));

			//view transforms
			glm::vec3 cp = glm::mat3(phys2VisTransform) * cameraPos;
			glm::vec3 cld = glm::mat3(phys2VisTransform) * cameraLookDirection;
			glm::vec3 cu = glm::mat3(phys2VisTransform) * cameraUp;

			view = glm::lookAt(cp, cld, cu);

			//associate view matric and shader uniform V
			glUniformMatrix4fv(defaultShader->uniform("V"), 1, GL_FALSE, glm::value_ptr(view));
			//associate projection matrix with shader uniform P
			glUniformMatrix4fv(defaultShader->uniform("P"), 1, GL_FALSE, glm::value_ptr(projection));

			//render object
			mVisibleWorld[i]->render();

		}
	}
	SDL_GL_SwapWindow(window);
}

void Game::turnCamLeft() {
	btTransform ct = pAtom->getWorldTransform();
	btQuaternion rot = ct.getRotation();
	btQuaternion q;
	q.setEuler(0.0, 0.0, 0.01);
	ct.setRotation(q*rot);
	pAtom->setWorldTransform(ct);
}

void Game::turnCamRight() {
	btTransform ct = pAtom->getWorldTransform();
	btQuaternion rot = ct.getRotation();
	btQuaternion q;
	q.setEuler(0.0, 0.0, (-1.0)*0.01f);
	ct.setRotation(q*rot);
	pAtom->setWorldTransform(ct);
}

void Game::chargeHit() {
	// Get forward direction and move monkey in that direction.
	// Rotation occurs about the Z-axis (which points up)
	btTransform ct = pAtom->getWorldTransform();
	btMatrix3x3 m = ct.getBasis();
	ct.setOrigin(ct.getOrigin() - chargeForce*m.getColumn(1));
	pAtom->setWorldTransform(ct);
	thePhysicsWorld->contactPairTest(pAtom, atom, collisionCallback);
	
	if (atom && collisionCallback.atomsCollided == true) {
		atomCollision();
	}

	
}

void Game::atomCollision()
{
	for (int i = 0; i < mVisibleWorld.size(); ++i) {
		if (mVisibleWorld[i]->isVisible() && mVisibleWorld[i]->getName() == "Atom_Sphere.001") {
			mVisibleWorld[i]->setVisibility(false);
			thePhysicsWorld->removeRigidBody(atom);
			thePhysicsWorld->updateAabbs();
			atom->activate(true);
			atom->applyForce(btVector3(1000.0f, 1000.0f, 1000.0f), btVector3(1000.0f, 1000.0f, 1000.0f));

			btTransform ct = atom->getWorldTransform();
			btMatrix3x3 m = ct.getBasis();
			ct.setOrigin(ct.getOrigin() - 10000*m.getColumn(1));
			atom->setWorldTransform(ct);
		}

	}
}
